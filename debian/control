Source: lua-cyrussasl
Section: interpreters
Priority: optional
Maintainer: Debian Lua Team <pkg-lua-devel@lists.alioth.debian.org>
Uploaders: Sergei Golovan <sgolovan@debian.org>
Build-Depends: debhelper-compat (= 12), dh-lua, libsasl2-dev
Standards-Version: 4.6.0
Homepage: http://github.com/JorjBauer/lua-cyrussasl
Vcs-Browser: https://salsa.debian.org/lua-team/lua-cyrussasl
Vcs-Git: https://salsa.debian.org/lua-team/lua-cyrussasl.git

Package: lua-cyrussasl
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends}, ${misc:Depends}
Provides: ${lua:Provides}
XB-Lua-Versions: ${lua:Versions}
Description: Cyrus SASL library for the Lua language
 This package contains the Lua bindings for the Cyrus SASL APIs.

Package: lua-cyrussasl-dev
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Section: libdevel
Depends: lua-cyrussasl (= ${binary:Version}), ${misc:Depends}
Provides: ${lua:Provides}
XB-Lua-Versions: ${lua:Versions}
Description: Cyrus SASL development files for the Lua language
 This package contains the development files of the lua Cyrus SASL library,
 useful to create a statically linked binary (like a C application or a
 standalone Lua interpreter).
 Documentation is also shipped within this package.
