lua-cyrussasl (1.1.0-5) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster (oldstable):
    + Build-Depends: Drop versioned constraint on dh-lua.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sat, 10 Dec 2022 11:51:32 +0000

lua-cyrussasl (1.1.0-4) unstable; urgency=medium

  * Team-maintain the package (Closes: #995541).
  * d/watch: Scan GitHub tags.

 -- Bastian Germann <bage@debian.org>  Sat, 12 Nov 2022 17:15:04 +0100

lua-cyrussasl (1.1.0-3) unstable; urgency=medium

  * Fix the debian/watch uscan control file.
  * Bump the standards version to 4.6.0.

 -- Sergei Golovan <sgolovan@debian.org>  Thu, 11 Nov 2021 21:21:45 +0300

lua-cyrussasl (1.1.0-2) unstable; urgency=medium

  * Add Lua 5.4 to the list of versions to build the package.
  * Switch build dependency on debhelper to the debhelper-compat virtual
    package.
  * Bump the Debhelper compatibility version to 12.
  * Bump the standards version to 4.5.0.

 -- Sergei Golovan <sgolovan@debian.org>  Wed, 08 Jul 2020 09:45:37 +0300

lua-cyrussasl (1.1.0-1) unstable; urgency=medium

  * New upstream version, compatible with Lua 5.1, 5.2 and 5.3
    (closes: #914536).
  * Acknowledge the NMU by Aurelian Jarno.
  * Bump the Debhelper compatibility version to 11.
  * Bump the standards version to 4.4.0.
  * Move the VCS headers in debian/control to Salsa.
  * Add myself to the uploaders list.
  * Fix the debian/watch uscan control file.
  * Fix format in debian/copyright.

 -- Sergei Golovan <sgolovan@debian.org>  Mon, 15 Jul 2019 18:43:34 +0300

lua-cyrussasl (1.0.0-6.1) unstable; urgency=medium

  * Non-maintainer upload.
  * debian/control: Drop explicit Pre-Depends on multiarch-support
    (Closes: #870550).

 -- Aurelien Jarno <aurel32@debian.org>  Sat, 20 Jan 2018 21:18:12 +0100

lua-cyrussasl (1.0.0-6) unstable; urgency=medium

  * Rebuild against Lua 5.1.5-7 (Closes: #723486)

 -- Enrico Tassi <gareuselesinge@debian.org>  Sat, 30 Aug 2014 21:54:51 +0200

lua-cyrussasl (1.0.0-5) unstable; urgency=low

  * Package moved to git
  * Transitional packages removed 

 -- Enrico Tassi <gareuselesinge@debian.org>  Thu, 19 Apr 2012 11:58:13 +0200

lua-cyrussasl (1.0.0-4) unstable; urgency=low

  * debian/compat set to 8
  * Copyright format 1.0 
  * Packages renamed according to the new Lua policy
  * Switch to dh-lua
  * Updated standards-version to 3.9.3, no changes 

 -- Enrico Tassi <gareuselesinge@debian.org>  Thu, 19 Apr 2012 11:51:51 +0200

lua-cyrussasl (1.0.0-3) unstable; urgency=low

  * Multi-Arch compliant (lua5.1-policy >= 32, dh >= 8.1.3)

 -- Enrico Tassi <gareuselesinge@debian.org>  Wed, 20 Jul 2011 22:30:47 +0200

lua-cyrussasl (1.0.0-2) unstable; urgency=low

  * Rebuild against lua5.1-policy-dev >= 27 (Closes: #622457)
  * Updated standards-version to 3.9.2, no changes

 -- Enrico Tassi <gareuselesinge@debian.org>  Sat, 23 Apr 2011 20:56:40 +0200

lua-cyrussasl (1.0.0-1) unstable; urgency=low

  * Initial release. (Closes: #590855)

 -- Enrico Tassi <gareuselesinge@debian.org>  Thu, 29 Jul 2010 16:53:07 +0200
